"""
This module may contain various information (constants, structures, etc.)
used by multiple other classes. For example: error codes, permission keys,
text templates. It's recommended to keep things simple in here and don't use
complicated logic or many imports.
"""

from enum import Enum

__all__ = ['ErrorCode']


class ErrorCode(Enum):
    TRANSPORT_FAILED = 'TRANSPORT_FAILED'
    NO_CONVERTER_FOUND = 'NO_CONVERTER_FOUND'
    CONVERSION_FAILED = 'CONVERSION_FAILED'
    NO_LOADER_FOUND = 'NO_LOADER_FOUND'
    REJECTED_BY_LOADER = 'REJECTED_BY_LOADER'
    LOADING_FAILED = 'LOADING_FAILED'
