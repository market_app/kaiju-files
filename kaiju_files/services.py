"""
This module SHOULD only contain imports of service classes.

The reason of this module is to feed it to the service class registry for fast
class creation, so it should only contain the imports you need to register
as services.
"""

from .files import FileService
